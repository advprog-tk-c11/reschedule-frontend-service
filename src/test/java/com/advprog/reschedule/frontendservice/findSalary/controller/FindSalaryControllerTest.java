package com.advprog.reschedule.frontendservice.findSalary.controller;

import com.advprog.reschedule.frontendservice.authentication.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(FindSalaryController.class)
public class FindSalaryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @Test
    public void whenCallFindSalaryUrlShouldReturnFindSalaryTemplate() throws Exception {
        mockMvc.perform(get("/findSalary?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("salary"))
                .andExpect(view().name("findSalary/salary"));
    }
}
