package com.advprog.reschedule.frontendservice.data.controller;

import com.advprog.reschedule.frontendservice.authentication.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DataController.class)
public class DataControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    private final String username = "dummy";

    @Test
    @WithMockUser(username)
    public void whenCallTeacherListUrlShouldReturnTeacherListTemplate() throws Exception {
        mockMvc.perform(get("/data/teacher-list"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("teacherList"))
                .andExpect(view().name("data/teacherList"));
    }

    @Test
    @WithMockUser(username)
    public void whenCallClassListUrlShouldReturnClassListTemplate() throws Exception {
        mockMvc.perform(get("/data/class-list"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("classList"))
                .andExpect(view().name("data/classList"));
    }

    @Test
    @WithMockUser(username)
    public void whenCallSessionListUrlShouldReturnSessionListTemplate() throws Exception {
        mockMvc.perform(get("/data/session-list"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("sessionList"))
                .andExpect(view().name("data/sessionList"));
    }
}
