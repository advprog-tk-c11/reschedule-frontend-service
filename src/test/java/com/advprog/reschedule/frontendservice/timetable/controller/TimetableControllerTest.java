package com.advprog.reschedule.frontendservice.timetable.controller;

import com.advprog.reschedule.frontendservice.authentication.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TimetableController.class)
public class TimetableControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @Test
    public void whenCallTimetableUrlShouldReturnTimetableTemplate() throws Exception {
        mockMvc.perform(get("/timetable"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("timeTable"))
                .andExpect(view().name("timetable/table"));
    }
}
