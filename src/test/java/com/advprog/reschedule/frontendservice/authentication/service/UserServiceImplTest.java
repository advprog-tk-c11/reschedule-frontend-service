package com.advprog.reschedule.frontendservice.authentication.service;

import com.advprog.reschedule.frontendservice.authentication.model.User;
import com.advprog.reschedule.frontendservice.authentication.model.UserRole;
import com.advprog.reschedule.frontendservice.authentication.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mockito.Mockito.lenient;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setUsername("Dummy");
        user.setPassword("password");
        userRepository.save(user);
    }

    @Test
    public void testSignUpUser() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        User newUser = userService.signUpUser(user);
        assertEquals(user.getUsername(), newUser.getUsername());
        assertTrue(passwordEncoder.matches("password", user.getPassword()));
        assertEquals(newUser.getAuthorities().size(), 1);
        assertTrue(newUser.isAccountNonExpired());
        assertTrue(newUser.isAccountNonLocked());
        assertTrue(newUser.isEnabled());
        assertTrue(newUser.isCredentialsNonExpired());
    }

    @Test
    public void testUpdateUserRole() {
        UserRole userRole = UserRole.TEACHER;
        userService.updateUserRole(userRole, user);
        assertEquals(user.getUserRole(), UserRole.TEACHER);
    }

    @Test
    public void testFindUserByUsername() {
        String otherUsername = "otherUser";
        boolean notFound = false;
        try {
            userService.loadUserByUsername(otherUsername);
        } catch (UsernameNotFoundException e) {
            notFound = true;
        };
        assertTrue(notFound);
    }

    @Test
    public void testUsernameExists() {
        assertTrue(!userService.usernameExists("otherUser"));
//        assertTrue(userService.usernameExists(user.getUsername()));
    }
}
