 package com.advprog.reschedule.frontendservice.addSalary.controller;

 import com.advprog.reschedule.frontendservice.addSalary.core.Classes;
 import com.advprog.reschedule.frontendservice.authentication.service.UserServiceImpl;
 import com.fasterxml.jackson.databind.ObjectMapper;
 import com.fasterxml.jackson.databind.ObjectWriter;
 import com.fasterxml.jackson.databind.SerializationFeature;
 import org.junit.jupiter.api.Test;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
 import org.springframework.boot.test.mock.mockito.MockBean;
 import org.springframework.http.MediaType;
 import org.springframework.security.test.context.support.WithMockUser;
 import org.springframework.test.web.servlet.MockMvc;

 import java.nio.charset.Charset;
 import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
 import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
 import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

 @WebMvcTest(controllers = AddSalaryController.class)
 public class AddSalaryControllerTest {

     @Autowired
     private MockMvc mockMvc;

     @MockBean
     private UserServiceImpl userService;

     private final String username = "dummy";

     @Test
     public void whenCallSimulationUrlShouldReturnSimulationTemplate() throws Exception{
         mockMvc.perform(get("/simulation"))
                 .andExpect(status().isOk())
                 .andExpect(handler().methodName("simulationHome"))
                 .andExpect(view().name("addSalary/simulation"));
     }

//     @Test
//     @WithMockUser(username)
//     public void whenCallNextDayUrlShouldReturnNextDayRedirect() throws Exception{
//
//         String postBody = "{\n" +
//                 " \"className\": \"class A\",\n" +
//                 " \"currentDay\": \"Senin\"\n" +
//                 "}";
//
//         mockMvc.perform(post("/simulation/next-day")
//                 .contentType(MediaType.APPLICATION_JSON_VALUE).content(postBody))
//                 .andExpect(handler().methodName("simulationNextDay"))
//                 .andExpect(status().is3xxRedirection())
//                 .andExpect(view().name("addSalary/simulation"));
//     }

     @Test
     public void whenCallLogsUrlShouldReturnLogsTemplate() throws Exception{
         mockMvc.perform(get("/simulation/logs"))
                 .andExpect(status().isOk())
                 .andExpect(handler().methodName("simulationLogs"))
                 .andExpect(view().name("addSalary/logs"));
     }
 }


