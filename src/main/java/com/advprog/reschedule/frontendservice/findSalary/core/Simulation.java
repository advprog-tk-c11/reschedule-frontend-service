package com.advprog.reschedule.frontendservice.findSalary.core;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class Simulation {
    String teacherNrg;
    LocalDate simulatedDate;
    long simulatedSalary;
}

