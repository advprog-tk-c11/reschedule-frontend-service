package com.advprog.reschedule.frontendservice.findSalary.controller;

import com.advprog.reschedule.frontendservice.findSalary.core.Simulation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(path = "/findSalary")
public class FindSalaryController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("")
    public String salary(@RequestParam(value = "nrg") String nrg, Model model) {
        model.addAttribute("simulation", restTemplate.getForObject("http://reschedule-backend-timetable.herokuapp.com/findSalary?nrg=" + nrg, Simulation.class));
        return "findSalary/salary";
    }
}
