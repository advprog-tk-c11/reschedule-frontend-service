package com.advprog.reschedule.frontendservice.authentication.service;

import com.advprog.reschedule.frontendservice.authentication.model.User;
import com.advprog.reschedule.frontendservice.authentication.model.UserRole;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User createUser(User user);

    User updateUserRole(UserRole userRole, User user);

    User signUpUser(User user);

    Boolean usernameExists(String username);
}
