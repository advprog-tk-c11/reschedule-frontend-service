package com.advprog.reschedule.frontendservice.authentication.model;

public enum UserRole {
    ADMIN, TEACHER
}
