package com.advprog.reschedule.frontendservice.authentication.handler;

import com.advprog.reschedule.frontendservice.authentication.model.User;

abstract class SignUpHandler {

    protected SignUpHandler nextHandler;

    public void setNext(SignUpHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    abstract void handle(User user);
}
