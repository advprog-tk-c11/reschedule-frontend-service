package com.advprog.reschedule.frontendservice.authentication.handler;

import com.advprog.reschedule.frontendservice.authentication.model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncodeHandler extends SignUpHandler {

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public void handle(User user) {
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);

        if (nextHandler != null) {
            nextHandler.handle(user);
        }
    }
}
