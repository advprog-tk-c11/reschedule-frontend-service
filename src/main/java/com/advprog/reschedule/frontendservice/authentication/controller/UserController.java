package com.advprog.reschedule.frontendservice.authentication.controller;

import com.advprog.reschedule.frontendservice.authentication.model.User;
import com.advprog.reschedule.frontendservice.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/signUp")
    public String signUp(Model model) {
        model.addAttribute("user", new User());
        return "authentication/signUp";
    }

    @PostMapping("/signUp")
    public String signUp(User user) {
        userService.signUpUser(user);
        return "redirect:/login";
    }

    @PostMapping("/checkUsernameExists")
    public @ResponseBody ResponseEntity checkUsernameExists(@RequestBody String username) {
        if (userService.usernameExists(username)) {
            return ResponseEntity.ok("Username Taken");
        }
        return ResponseEntity.ok("Username Available");
    }
}
