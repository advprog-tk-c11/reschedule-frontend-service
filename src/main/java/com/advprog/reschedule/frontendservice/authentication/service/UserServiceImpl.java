package com.advprog.reschedule.frontendservice.authentication.service;

import com.advprog.reschedule.frontendservice.authentication.handler.PasswordEncodeHandler;
import com.advprog.reschedule.frontendservice.authentication.model.User;
import com.advprog.reschedule.frontendservice.authentication.model.UserRole;
import com.advprog.reschedule.frontendservice.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private PasswordEncodeHandler passwordEncodeHandler = new PasswordEncodeHandler();

    @Override
    public User createUser(User user) {
        user = new User(user.getUsername(), user.getPassword());
        userRepository.save(user);
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (usernameExists(username)) {
            return userRepository.findByUsername(username);
        } else {
            throw new UsernameNotFoundException(String.format("Cannot find user with username %s.", username));
        }
    }

    public User signUpUser(User user) {
        passwordEncodeHandler.handle(user);
        User newUser = createUser(user);
        return newUser;
    }

    @Override
    public User updateUserRole(UserRole userRole, User user) {
        user.setUserRole(userRole);
        userRepository.save(user);
        return user;
    }

    @Override
    public Boolean usernameExists(String username) {
        return (userRepository.findByUsername(username) != null);
    }
}
