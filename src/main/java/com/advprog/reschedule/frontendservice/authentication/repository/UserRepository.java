package com.advprog.reschedule.frontendservice.authentication.repository;

import com.advprog.reschedule.frontendservice.authentication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    User findByUsername(String username);
}
