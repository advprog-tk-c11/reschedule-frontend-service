package com.advprog.reschedule.frontendservice.timetable.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/timetable")
public class TimetableController {
    @RequestMapping(method = RequestMethod.GET, value = "")
    public String timeTable() {
        return "timetable/table";
    }
}
