package com.advprog.reschedule.frontendservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {
    @RequestMapping(method = RequestMethod.GET, value = "")
    private String landingPage() {
        return "landingPage";
    }
}
