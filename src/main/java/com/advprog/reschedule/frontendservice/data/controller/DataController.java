package com.advprog.reschedule.frontendservice.data.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/data")
public class DataController {
    @RequestMapping(method = RequestMethod.GET, value = "/teacher-list")
    public String teacherList() {
        return "data/teacherList";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/class-list")
    public String classList() {
        return "data/classList";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/session-list")
    public String sessionList() {
        return "data/sessionList";
    }
}
