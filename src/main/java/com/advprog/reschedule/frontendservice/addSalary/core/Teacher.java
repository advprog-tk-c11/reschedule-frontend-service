// Soon it weel be deleted, becuase timetable feature not need it anymore but other feature still implement it
package com.advprog.reschedule.frontendservice.addSalary.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Teacher {
    private String nrg;

    private String name;

    private long totalSalary;

    private List<Session> workOnSessions;
}
