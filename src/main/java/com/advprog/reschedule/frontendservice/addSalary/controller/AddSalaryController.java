package com.advprog.reschedule.frontendservice.addSalary.controller;

import com.advprog.reschedule.frontendservice.addSalary.core.Classes;
import com.advprog.reschedule.frontendservice.addSalary.core.Teacher;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/simulation")
public class AddSalaryController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("")
    public String simulationHome(Model model) {
        model.addAttribute("classes", restTemplate.getForObject("http://reschedule-backend-timetable.herokuapp.com/timetable/allClass", Classes[].class));
        return "addSalary/simulation";
    }

    @PostMapping("/next-day")
    public String simulationNextDay(HttpServletRequest request) {
        Classes newClass = new Classes();
        String className = request.getParameter("className");
        String currentDay = request.getParameter("currentDay");
        newClass.setName(className);
        newClass.setCurrentDay(currentDay);
        restTemplate.postForObject("http://reschedule-backend-timetable.herokuapp.com/timetable/updateClass", newClass, Classes.class);
        System.out.println("next-day");
        return "redirect:/simulation";
    }

    @GetMapping("/logs")
    public String simulationLogs(Model model) {
        model.addAttribute("classes", restTemplate.getForObject("http://reschedule-backend-timetable.herokuapp.com/timetable/allClass", Classes[].class));
        model.addAttribute("teachers", restTemplate.getForObject("http://reschedule-backend-timetable.herokuapp.com/timetable/allTeacher", Teacher[].class));
        return "addSalary/logs";
    }
}
