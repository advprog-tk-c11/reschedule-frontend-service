// Soon it weel be deleted, becuase timetable feature not need it anymore but other feature still implement it
package com.advprog.reschedule.frontendservice.addSalary.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;

@Setter
@Getter
@NoArgsConstructor
public class Session {
    private int id;

    private String course;

    private String day;

    private LocalTime startTime;

    private LocalTime endTime;

    private Teacher teacher;

    private Classes onClass;
}