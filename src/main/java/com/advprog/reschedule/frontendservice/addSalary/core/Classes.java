// Soon it weel be deleted, becuase timetable feature not need it anymore but other feature still implement it
package com.advprog.reschedule.frontendservice.addSalary.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Classes {
    private String name;
    private List<Session> availableSessions;
    private String currentDay;
}
