$(document).ready(function () {

        let usernameExists = false;

        $('#username').change(function (event) {
            checkUsernameExists();
        });
});

let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");

$(document).ajaxSend(function(e, xhr, options) {
    xhr.setRequestHeader(header, token);
});

function checkUsernameExists() {
    $("#usernameExists").html("").hide()

    var username = $("#username").val();

    $.ajax({
        type : "POST",
        url : "/checkUsernameExists",
        data : JSON.stringify(username),
        dataType: 'json',
        contentType : "application/json",
        success : function (data) {
            if (data == "Username Taken") {
                usernameExists = true;
                $('#submit-button').attr('disabled', true);
            } else {
                usernameExists = false;
                $('#submit-button').attr('disabled', false);
            }
            $("#usernameExists").show().html(data);
        }
    });
}

