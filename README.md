# Advance Programming 2020/2021 Semester Genap

[![pipeline status](https://gitlab.com/advprog-tk-c11/reschedule-frontend-service/badges/master/pipeline.svg)](https://gitlab.com/advprog-tk-c11/reschedule-frontend-service/-/commits/master)

[![coverage report](https://gitlab.com/advprog-tk-c11/reschedule-frontend-service/badges/master/coverage.svg)](https://gitlab.com/advprog-tk-c11/reschedule-frontend-service/-/commits/master)


## Kelompok C11

| No. | Nama                      | NPM        |
|-----|---------------------------|------------|
| 1   | Muhamad Andre Gunawan     | 1906400021 |
| 2   | Erica Harlin              | 1906351013 |
| 3   | Johanes Jason             | 1906293120 |
| 4   | Olivia Monica             | 1906350641 |

## Re:schedule

### Deskripsi
Sebuah web penjadwalan jadwal belajar selama satu semester yang dapat digunakan oleh guru pada jenjang pendidikan sebelum kuliah, dan juga simulator perhitungan gaji guru berdasarkan jadwal belajar yang ada.

### Fitur
1. Timetable : Menampilkan jadwal belajar selama satu semester
2. Add Salary : Simulasi ganti hari pada suatu kelas, yang berdampak kepada penambahan gaji guru
3. Find Salary : Simulasi prediksi gaji guru beberapa hari kedepan, berdasarkan sesi yang diampu oleh guru tersebut
4. Authentication : User role, dimana hanya admin yang berhak untuk menambahkan jadwal belajar

### Design Pattern
1. Timetable : Compound Pattern
2. Add Salary : Observer Pattern
3. Find Salary : Template Pattern
4. Authentication : Chain Of Responsibility Pattern

### Services
1. Front End Services (Frontend, Authentication): [https://gitlab.com/advprog-tk-c11/reschedule-frontend-service](https://gitlab.com/advprog-tk-c11/reschedule-frontend-service)

2. Back End Services (Timetable, Add Salary, Find Salary): [https://gitlab.com/advprog-tk-c11/reschedule-backend-timetable-service](https://gitlab.com/advprog-tk-c11/reschedule-backend-timetable-service)

### Pembagian Tugas
1. Timetable : Muhamad Andre Gunawan
2. Add Salary : Johanes Jason
3. Find Salary : Erica Harlin
4. Authentication : Olivia Monica